/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 11:10:01 by a42               #+#    #+#             */
/*   Updated: 2020/12/12 01:49:21 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_one.h"

void		destroy_mutex(t_data data, int n)
{
	while (--n >= 0)
		pthread_mutex_destroy(&data.forks[n]);
	pthread_mutex_destroy(&data.write);
}

int			mutex_init(t_data *d)
{
	int		i;

	if (!(d->forks = malloc(sizeof(pthread_mutex_t) * d->n_ph)))
		return (msg_error("mutex_init(0) : erreur malloc forks\n"));
	i = -1;
	while (++i < d->n_ph)
		if (pthread_mutex_init(&d->forks[i], NULL))
			return (msg_error("mutex_init(1) : erreur mutex init forks\n"));
	if (pthread_mutex_init(&d->write, NULL))
		return (msg_error("mutex_init(2) : erreur mutex init write\n"));
	return (SUCCESS);
}

t_philo		**create_philosophers(t_data *d)
{
	int		i;
	t_philo	**ph;

	if (!(ph = (t_philo **)malloc(sizeof(t_philo *) * d->n_ph)))
		return (NULL);
	i = -1;
	while (++i < d->n_ph)
	{
		ph[i] = (t_philo *)malloc(sizeof(t_philo));
		ph[i]->id = i + 1;
		ph[i]->n_eat = 0;
		ph[i]->t_last_eat = 0;
		ph[i]->data = d;
		ph[i]->f_left = &d->forks[i];
		ph[i]->f_right = &d->forks[(i + 1) % d->n_ph];
		pthread_mutex_init(&ph[i]->is_eating, NULL);
	}
	return (ph);
}

int			main(int ac, char **av)
{
	t_data	d;
	t_philo	**p;

	if (parsing(ac, av, &d))
		return (FAILURE);
	if (mutex_init(&d))
		return (FAILURE);
	if (!(p = create_philosophers(&d)))
		return (FAILURE);
	if (create_threads(p, &d))
		return (FAILURE);
	if (join_threads(p, &d))
		return (FAILURE);
	destroy_mutex(d, d.n_ph);
	free_all_malloc(&d, p);
	return (SUCCESS);
}
