/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 14:20:25 by a42               #+#    #+#             */
/*   Updated: 2020/12/12 02:42:14 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_one.h"

void			*eat(t_philo *p)
{
	((p->id % 2) == 0) ? pthread_mutex_lock(p->f_left) : 0;
	pthread_mutex_lock(p->f_right);
	screen_msg(p, FORK);
	((p->id % 2) == 1) ? pthread_mutex_lock(p->f_left) : 0;
	if (!p->data->dead)
	{
		pthread_mutex_lock(&p->is_eating);
		screen_msg(p, FORK);
		screen_msg(p, EAT);
		p->t_last_eat = get_time();
		wait_time_to(p->data->tt_eat);
		++p->n_eat;
		pthread_mutex_unlock(&p->is_eating);
	}
	pthread_mutex_unlock(p->f_left);
	pthread_mutex_unlock(p->f_right);
	return (NULL);
}

void			*phil_life_controller(void *p)
{
	t_philo *phil;

	phil = (t_philo *)p;
	while (!phil->data->dead && phil->n_eat != phil->data->n_max_eat)
	{
		pthread_mutex_lock(&phil->is_eating);
		// write(1, "1\n", 2);
		if (!phil->data->dead && phil->n_eat != phil->data->n_max_eat &&
			get_time() - phil->t_last_eat > (u_int64_t)phil->data->tt_die)
		{		
			// write(1, "2\n", 2);
			screen_msg(phil, DEAD);
			phil->data->dead = 1;
			break ;
		}
		pthread_mutex_unlock(&phil->is_eating);
		usleep(100);
	}
	pthread_mutex_unlock(phil->f_left);
	pthread_mutex_unlock(phil->f_right);
	pthread_mutex_unlock(&phil->is_eating);
	return (NULL);
}

void			*phil_start(void *p)
{
	t_philo		*phil;
	pthread_t	control;

	phil = (t_philo *)p;
	pthread_create(&control, NULL, phil_life_controller, phil);
	while (!phil->data->go)
		;
	while (!phil->data->dead)
	{
		eat(phil);
		if (!phil->data->dead && phil->n_eat == phil->data->n_max_eat)
		{
			pthread_mutex_lock(&phil->data->write);
			phil->data->no_hungry++;
			pthread_mutex_unlock(&phil->data->write);
			break ;
		}
		screen_msg(p, SLEEP);
		wait_time_to(phil->data->tt_sleep);
		screen_msg(p, THINK);
	}
	pthread_join(control, NULL);
	pthread_mutex_destroy(&phil->is_eating);
	return (NULL);
}

int				create_threads(t_philo **p, t_data *d)
{
	int			i;

	i = -1;
	while (++i < d->n_ph)
		if (pthread_create(&(p[i]->thread), NULL, &phil_start, (void *)p[i]))
			return (msg_error("create_thread(0) : erreur création thread\n"));
	d->go = 1;
	get_time();
	return (SUCCESS);
}

int				join_threads(t_philo **p, t_data *d)
{
	int			i;

	while (!d->dead && d->no_hungry < d->n_ph)
		usleep(100);
	i = -1;
	while (++i < d->n_ph)
		if (pthread_join(p[i]->thread, NULL))
			return (msg_error("join_threads(0) : erreur join thread\n"));
	return (SUCCESS);
}
