# philosophers

Comprendre le fonctionnement des threads dans le langage C et les utiliser pour résoudre un problème logique.
La simulation s'arrête si tous les philosophes ont mangé suffisamment ou si un philosophe meurt.

Il existe trois versions différentes de l'algorithme :
- Philo_one utilise les threads et les mutexs.
- Philo_two utilise des threads et des sémaphores.
- Philo_three utilise des processus et des sémaphores.

Lire le [sujet][1]

`Ce projet a été codé pour MACOS`

### Construire et lancer le projet

1. Télécharger / Cloner le repo

```
git clone https://gitlab.com/flmarsil42/philosophers.git
```

2. `cd` dans le dossier racine, puis`cd` dans un dossier philo_x et utiliser la commande `make`

```
cd philosophers/philo_one
make
```

3.  run `./philo_x arg1 arg2 arg3 arg4 (arg5)` depuis le dossier philo_x.
    - arg1 représente le nombre de philosophes
    - arg2 représente la durée de vie des philosophes en millisecondes
    - arg3 représente le temps que mettent les philosophes à manger en millisecondes
    - arg4 représente le temps que mettent les philosophes à dormir en millisecondes
    - arg5 (optionnel) représente le nombre de fois que dois manger chaque philosophes avant que la simulation ne s'arrête d'elle même
    
## Sources

- [Programmation système en C sous UNIX (FR)][5]
- [Processus et threads (FR)][2]
- [Cours sur les threads (FR)][3]
- [Optimizing philo algo][4]
- [Comprendre les sémaphores (FR)][6]
- [How to use sempahores on fork processes][7]

[1]: https://gitlab.com/flmarsil42/philosophers/-/blob/master/en.subject.pdf
[2]: https://zestedesavoir.com/tutoriels/607/les-systemes-dexploitation/processus-et-threads/
[3]: https://cours.polymtl.ca/inf2610/documentation/notes/chap4.pdf
[4]: https://www.notion.so/philosophers-VM-c60be9c836084edfbcd9c07e29b429c4
[5]: https://pub.phyks.me/sdz/sdz/la-programmation-systeme-en-c-sous-unix.html#Lesthreads
[6]: https://sites.uclouvain.be/SystInfo/notes/Theorie/html/Threads/coordination.html
[7]: https://stackoverflow.com/questions/16400820/how-to-use-posix-semaphores-on-forked-processes-in-c
