/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 11:10:01 by a42               #+#    #+#             */
/*   Updated: 2020/12/10 10:57:18 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philo_three.h"

int			destroy_semaphores(void)
{
	int		ret;

	ret = 0;
	if ((ret = sem_close(g_forks)) && ret < 0)
		return (msg_error("destroy_semaphores(0): error sem_close(forks)\n"));
	if ((ret = sem_close(g_write)) && ret < 0)
		return (msg_error("destroy_semaphores(1): error sem_close(write)\n"));
	if ((ret = sem_close(g_pick)) && ret < 0)
		return (msg_error("destroy_semaphores(2): error sem_close(pick)\n"));
	if ((ret = sem_close(g_dead)) && ret < 0)
		return (msg_error("destroy_semaphores(4): error sem_close(pick)\n"));
	if ((ret = sem_unlink(S_FORKS)) && ret < 0)
		return (msg_error("destroy_semaphores(3): error sem_unlink(forks)\n"));
	if ((ret = sem_unlink(S_PICK)) && ret < 0)
		return (msg_error("destroy_semaphores(5): error sem_unlink(pick)\n"));
	if ((ret = sem_unlink(S_WRITE)) && ret < 0)
		return (msg_error("destroy_semaphores(6): error sem_unlink(write)\n"));
	if ((ret = sem_unlink(S_DIE)) && ret < 0)
		return (msg_error("destroy_semaphores(7): error sem_unlink(die)\n"));
	return (SUCCESS);
}

int			semaphores_init(t_data *d)
{
	sem_unlink(S_FORKS);
	sem_unlink(S_PICK);
	sem_unlink(S_WRITE);
	sem_unlink(S_DIE);
	if ((g_forks = sem_open(S_FORKS, O_CREAT, 0644, d->n_ph)) == SEM_FAILED)
		return (msg_error("sem_init(0) : erreur sem_open(S_FORKS)\n"));
	if ((g_write = sem_open(S_WRITE, O_CREAT, 0644, 1)) == SEM_FAILED)
		return (msg_error("sem_init(1) : erreur sem_open(S_WRITE)\n"));
	if ((g_pick = sem_open(S_PICK, O_CREAT, 0644, 1)) == SEM_FAILED)
		return (msg_error("sem_init(2) : erreur sem_open(S_PICK)"));
	if ((g_dead = sem_open(S_DIE, O_CREAT, 0644, 1)) == SEM_FAILED)
		return (msg_error("sem_init(2) : erreur sem_open(S_PICK)"));
	return (SUCCESS);
}

pid_t		*main_process(t_data d)
{
	int		i;
	t_philo	ph;
	pid_t	*ph_pid;

	if (!(ph_pid = (pid_t *)malloc(sizeof(pid_t) * d.n_ph)))
		return (NULL);
	i = -1;
	while (++i < d.n_ph)
	{
		ph.id = i + 1;
		ph.data = d;
		ph.n_eat = 0;
		ph.t_last_eat = 0;
		ph.sem_id[0] = ph.id + '0';
		ph.sem_id[1] = '\0';
		sem_unlink(ph.sem_id);
		if ((ph.eating = sem_open(ph.sem_id, O_CREAT, 0644, 1))
			== SEM_FAILED)
			return (NULL);
		if ((ph_pid[i] = fork()) < 0)
			exit(msg_error("main_process(1) : erreur fork\n"));
		if (!ph_pid[i])
			phil_start(ph, ph_pid);
	}
	return (ph_pid);
}

void		wait_child_process(t_data *d, pid_t *main_pid)
{
	int		i;
	int		dead;

	dead = 0;
	while (!dead)
	{
		if (waitpid(-1, &dead, 0) < 0 || ((WIFEXITED(dead)
			|| WIFSIGNALED(dead)) && dead))
		{
			i = -1;
			while (++i < d->n_ph)
				kill(main_pid[i], SIGINT);
			break ;
		}
	}
}

int			main(int ac, char **av)
{
	t_data	d;
	pid_t	*main_pid;

	if (parsing(ac, av, &d))
		return (FAILURE);
	if (semaphores_init(&d))
		return (FAILURE);
	if (!(main_pid = main_process(d)))
		return (FAILURE);
	wait_child_process(&d, main_pid);
	if (destroy_semaphores())
		return (FAILURE);
	free(main_pid);
	return (SUCCESS);
}
